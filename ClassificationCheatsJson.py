import json
import sys

from ksp_savefile_parser import parse_savefile

# Partially based on scripts of OndrikB

DEBUGMODE = False

KERBOL_SYSTEM = {
    "Kerbol": dict(parent=None, id=0),
    "Moho": dict(parent="Kerbol", id=4),
    "Eve": dict(parent="Kerbol", id=5),
    "Gilly": dict(parent="Eve", id=13),
    "Kerbin": dict(parent="Kerbol", id=1),
    "Mun": dict(parent="Kerbin", id=2),
    "Minmus": dict(parent="Kerbin", id=3),
    "Duna": dict(parent="Kerbol", id=6),
    "Ike": dict(parent="Duna", id=7),
    "Dres": dict(parent="Kerbol", id=15),
    "Jool": dict(parent="Kerbol", id=8),
    "Laythe": dict(parent="Jool", id=9),
    "Vall": dict(parent="Jool", id=10),
    "Tylo": dict(parent="Jool", id=11),
    "Bop": dict(parent="Jool", id=12),
    "Pol": dict(parent="Jool", id=14),
    "Eeloo": dict(parent="Kerbol", id=16)
}


CAPACITIES = {
    "Mark1Cockpit": 1,
    "Mark2Cockpit": 1,
    "mk2Cockpit.Standard": 2,
    "mk2Cockpit.Inline": 2,
    "mk3Cockpit.Shuttle": 4,
    "mk1pod.v2": 1,
    "mk1-3pod": 3,
    "landerCabinSmall": 1,
    "mk2LanderCabin.v2": 2,
    "cupola": 1,
    "seatExternalCmd": 1,
    "mk3CrewCabin": 16,
    "mk2CrewCabin": 4,
    "MK1CrewCabin": 2,
    "crewCabin": 4,
    "Large.Crewed.Lab": 2
}

DENSITIES = {
    "LiquidFuel": 5,
    "Oxidizer": 5,
    "IntakeAir": 5,
    "SolidFuel": 7.5,
    "MonoPropellant": 4,
    "XenonGas": 0.1,
    "Ore": 10,
    "EVA Propellant": 0,
    "ElectricCharge": 0,
    "Ablator": 1
}

SIZES = {
    "none": "N",
    "tiny": "T",
    "small": "S",
    "medium": "M",
    "large": "L",
    "extra_large":  "X",
    "enormous": "E"
}


VALID_RESOURCES = (
    "LiquidFuel",
    "Oxidizer",
    "MonoPropellant",
    "XenonGas"
)

MOD_BLACKLIST = ()

CLAW = "GrapplingDevice"
DRILLS = ("MiniDrill", "RadialDrill")
ISRUS = ("MiniISRU", "ISRU")

CREW_BLACKLIST = ("Jebediah Kerman", "Bill Kerman", "Bob Kerman", "Valentina Kerman")

DOCKING_PORTS = {
    "dockingPort3": 0,
    "dockingPort2": 1,
    "dockingPortLarge": 1,
    "dockingPortLateral": 1,
    "mk2DockingPort": 1,
    "dockingPort1": 2
}

MONOPROPELLANT_THRESHHOLD = 6   # in tonns
XENON_THRESHHOLD = 1            # in tonns


class Craft:
    def __init__(self, craft_dict):
        self.raw = craft_dict
        self.name = craft_dict["name"]
        self.parts_search = craft_dict["PART"]

        searched_id = int(craft_dict["ORBIT"]["REF"])
        self.body = None

        for name, data in KERBOL_SYSTEM.items():
            if data.get("id") == searched_id:
                self.body = name

        if self.body is None:
            sys.stderr("Celestrial id ({}) does not exist".format(searched_id))
            sys.exit(3)

        self.logs = []
        self.part_count = len(self.parts_search)
        self.has_drill, self.has_isru, self.has_claw = False, False, False
        self.docking_ports = [0, 0, 0]
        self.crew_capacity = 0
        self.mass = 0.0
        self.resources = {
            "LiquidFuel": 0,
            "Oxidizer": 0,
            "IntakeAir": 0,
            "SolidFuel": 0,
            "MonoPropellant": 0,
            "XenonGas": 0,
            "Ore": 0,
            "EVA Propellant": 0,
            "ElectricCharge": 0,
            "Ablator": 0
        }

        self.valid = True
        self.invalidation_resons = []
        self.upsilon_name = ""

    def check_craft(self):
        """
        :param craft: The craft as a dictionary derived from parsed save file
        :return: A tuple, containing mass, crew capacity, resources and part count
        """

        for part in self.parts_search:
            # Check if none of the "main four" is on the craft
            if "crew" in part:
                if any(kerbal in part["crew"] for kerbal in CREW_BLACKLIST):
                    self.valid = False
                    self.invalidation_resons.append("Must not contain one of the \"main\" Kerbals")

                    self.mass += float(part["mass"])
            if "RESOURCE" in part:
                if isinstance(part["RESOURCE"], list):
                    for resource in part["RESOURCE"]:
                        self.mass += DENSITIES[resource["name"]] * float(resource["amount"]) / 1000
                        self.resources[resource["name"]] += DENSITIES[resource["name"]] * float(resource["amount"]) / 1000
                else:
                    resource = part["RESOURCE"]
                    self.mass += DENSITIES[resource["name"]] * float(resource["amount"]) / 1000
                    self.resources[resource["name"]] += DENSITIES[resource["name"]] * float(resource["amount"]) / 1000
            if part["name"] in CAPACITIES:
                self.crew_capacity += CAPACITIES[part["name"]]
            elif part["name"] == CLAW:
                self.has_claw = True
            elif part["name"] in DRILLS:
                self.has_drill = True
            elif part["name"] in ISRUS:
                self.has_isru = True
            elif part["name"] in DOCKING_PORTS:
                if isinstance(part["MODULE"], list):
                    for mod in part["MODULE"]:
                        if mod["name"] == "ModuleDockingNode":
                            if mod["state"] == "Ready":
                                self.docking_ports[DOCKING_PORTS[part["name"]]] += 1
                else:
                    mod = part["MODULE"]
                    if mod["name"] == "ModuleDockingNode":
                        if mod["state"] == "Ready":
                            self.docking_ports[DOCKING_PORTS[part["name"]]] += 1

    def is_cheating(self):
        """
        :param craft: The craft as a dictionary derived from parsed save file
        :param log_file_path: The path to the log file
        :return: A "cheat number", ranging from 1 to -infinity
        The higher it is, the higher the probability that the player cheated
        """
        trip_logger_modules = []

        for part in self.parts_search:
            if "MODULE" in part:
                if part["name"] != "kerbalEVA":
                    if isinstance(part["MODULE"], list):
                        for mod in part["MODULE"]:
                            if mod["name"] == "ModuleTripLogger":
                                trip_logger_modules.append(mod)
                    else:
                        if part["MODULE"]["name"] == "ModuleTripLogger":
                            trip_logger_modules.append(part["MODULE"])

        raw_logs = []

        for i, mod in enumerate(trip_logger_modules):
            self.logs.append("Trip log {} \n {} \n".format(i + 1, mod["Log"][mod["Log"]["flight"]]))
            raw_logs.append(mod["Log"][mod["Log"]["flight"]])

        # Ensures, every log is there once
        filtered_logs = [li.split("\n") for li in list(set(["\n".join(log) for log in raw_logs]))]

        parent = KERBOL_SYSTEM.get(self.body, None).get("parent")

        if parent == "Kerbol":
            parent = "Sun"

        self.cheat = 1
        cheat_detect = 1
        if self.body == "Kerbin":
            cheat_detect = 0
            cheat = 0

        # auto-catching
        if cheat_detect == 1:
            for log in filtered_logs:
                if isinstance(log, list):
                    for sub_log in log:
                        if self.body != "Kerbin" and sub_log == "Flyby," + self.body:
                            self.cheat -= 1
                        if self.body != "Kerbin" and self.body != "Mun" and self.body != "Minmus" and sub_log == "Flyby," + parent:
                            self.cheat -= 1
                        if parent != "Sun" and sub_log == "Flyby,Sun":
                            self.cheat -= 1
                else:
                    self.cheat = 1

        self.cheat_conclusion = "Requires manual checking"
        if self.cheat == 1:
            self.cheat_conclusion = "Cheated beyond resonable doubt"
            self.valid = False
            self.invalidation_resons = "Flight profile makes no sense"
        elif self.cheat <= -1:
            self.cheat_conclusion = "Probability of cheating low"

        return self.cheat

    def classify_craft(self):
        # Parts
        if self.part_count < 50:
            self.upsilon_name += SIZES["tiny"]
        elif self.part_count < 150:
            self.upsilon_name += SIZES["small"]
        elif self.part_count < 300:
            self.upsilon_name += SIZES["medium"]
        elif self.part_count < 500:
            self.upsilon_name += SIZES["large"]
        elif self.part_count < 800:
            self.upsilon_name += SIZES["extra_large"]
        else:
            self.upsilon_name += SIZES["enormous"]

        # Fuel
        fuel = sum(value if name in VALID_RESOURCES else 0 for name, value in self.resources.items())
        if fuel == 0:
            self.upsilon_name += SIZES["none"]
        elif fuel < 10:
            self.upsilon_name += SIZES["tiny"]
        elif fuel < 40:
            self.upsilon_name += SIZES["small"]
        elif fuel < 200:
            self.upsilon_name += SIZES["medium"]
        elif fuel < 1000:
            self.upsilon_name += SIZES["large"]
        elif fuel < 4000:
            self.upsilon_name += SIZES["extra_large"]
        else:
            self.upsilon_name += SIZES["enormous"]

        # Crew
        if self.crew_capacity == 0:
            self.upsilon_name += SIZES["none"]
        elif self.crew_capacity < 5:
            self.upsilon_name += SIZES["tiny"]
        elif self.crew_capacity < 10:
            self.upsilon_name += SIZES["small"]
        elif self.crew_capacity < 20:
            self.upsilon_name += SIZES["medium"]
        elif self.crew_capacity < 50:
            self.upsilon_name += SIZES["large"]
        elif self.crew_capacity < 100:
            self.upsilon_name += SIZES["extra_large"]
        else:
            self.upsilon_name += SIZES["enormous"]

        self.upsilon_name += "-"

        self.upsilon_name += "1" if self.has_drill else "0"
        self.upsilon_name += "1" if self.has_isru else "0"

        self.upsilon_name += "-"

        for port in self.docking_ports:
            self.upsilon_name += str(min(port, 9))

        if self.has_claw or self.resources["XenonGas"] >= XENON_THRESHHOLD or self.resources["MonoPropellant"] >= MONOPROPELLANT_THRESHHOLD:
            self.upsilon_name += "-"

        if self.has_claw:
            self.upsilon_name += "k"

        if self.resources["XenonGas"] >= XENON_THRESHHOLD:
            self.upsilon_name += "x"

        if self.resources["MonoPropellant"] >= MONOPROPELLANT_THRESHHOLD:
            self.upsilon_name += "m"

    def get_dict(self):
        dict_ = {
            "raw": self.raw,
            "original name": self.name,
            "upsilon name": self.upsilon_name,
            "cheat probability": self.cheat,
            "conclusion": self.cheat_conclusion,
            "valid": self.valid,
            "invalidation reasons": self.invalidation_resons,
            "celestial": self.body,
        }
        return dict_


def find_craft(pst_dict):
    """
    :param pst_dict: The parsed savefile (a dict)
    :return: The craft whose name contains [UPSILON] as a dict
    """

    craft_searches = []
    for vessel in pst_dict["GAME"]["FLIGHTSTATE"]["VESSEL"]:
        if "[UPSILON]" in vessel["name"]:
            if vessel["type"] != "Debris":
                craft_searches.append(vessel)

    return [Craft(d) for d in craft_searches]


def control_save_file(save_file):
    loader_info = save_file["GAME"]["LoaderInfo"]
    for mod in loader_info:
        if any(bl in mod for bl in MOD_BLACKLIST) and loader_info[mod] == "True":
            sys.stderr.write("Illegal mod used: " + mod)
            sys.exit(3)


def main(sfs_in_text):
    """
    :param save_file_path: The path of the save file
    :return: The proper json representation
    """
    try:
        pst_dict = parse_savefile(sfs_in_text)
    except Exception as e:
        sys.stderr.write("Parser error: \"" + repr(e) + "\"\n")
        exit(2)

    control_save_file(pst_dict)

    crafts = find_craft(pst_dict)

    tot_dict = []

    for craft in crafts:
        craft.is_cheating()
        craft.check_craft()
        craft.classify_craft()
        tot_dict.append(craft.get_dict())

    # Change indent to None if you don"t need the human readable form
    json_string = json.dumps(tot_dict, indent=2)

    return json_string


if __name__ == "__main__":
    try:
        sys.stdout.write(main(sys.stdin.read()))
    except Exception as e:
        sys.stderr.write("Uncaught python error: \"" + repr(e) + "\"\n")
        exit(1)
    else:
        sys.stderr.write("It seems, everything is fine")
        exit(0)
