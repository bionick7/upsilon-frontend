# Yes, you're very clever.

.PHONY: clean

style.css: colours.less

%.css: %.less
	lessc $< $@ --no-color --clean-css

clean:
	rm -rf style.css
